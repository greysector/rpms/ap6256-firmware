Summary: Wireless chipset (AP6256) firmware
Name: ap6256-firmware
Version: 7.45.96.61
Release: 3
URL: https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware
Source0: https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware/-/raw/master/fw_bcm43456c5_ag.bin
Source1: https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware/-/raw/master/brcmfmac43456-sdio.clm_blob
Source2: https://gitlab.manjaro.org/manjaro-arm/packages/community/ap6256-firmware/-/raw/master/nvram_ap6256.txt
Source3: https://github.com/armbian/firmware/raw/master/BCM4345C5.hcd
License: Redistributable, no modification permitted
BuildArch: noarch
Requires: linux-firmware

%description
Firmware for the wireless chipset in Rock Pi 4, Rock64, Pinebook Pro and others.

%prep
%setup -qcT

%build

%install
install -dm755 %{buildroot}/lib/firmware/brcm
install -pm644 %{S:0} %{buildroot}/lib/firmware/brcm/brcmfmac43456-sdio.bin
install -pm644 %{S:1} %{buildroot}/lib/firmware/brcm/
install -Dm644 %{S:2} %{buildroot}/lib/firmware/brcm/brcmfmac43456-sdio.txt
for hw in radxa,{rockpi4{b,c},zero} pine64,{pinebook-pro,rockpro64-v2.1,quartz64-{a,b}} rockchip,rk3399-orangepi ; do
    for type in bin txt ; do
        ln -s brcmfmac43456-sdio.${type} %{buildroot}/lib/firmware/brcm/brcmfmac43456-sdio.${hw}.${type}
    done
done
install -pm644 %{S:3} %{buildroot}/lib/firmware/brcm/

%files
/lib/firmware/brcm/brcmfmac43456-sdio.bin
/lib/firmware/brcm/brcmfmac43456-sdio.clm_blob
/lib/firmware/brcm/brcmfmac43456-sdio.txt
/lib/firmware/brcm/brcmfmac43456-sdio.radxa,rockpi4b.bin
/lib/firmware/brcm/brcmfmac43456-sdio.radxa,rockpi4c.bin
/lib/firmware/brcm/brcmfmac43456-sdio.radxa,zero.bin
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,pinebook-pro.bin
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,rockpro64-v2.1.bin
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,quartz64-a.bin
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,quartz64-b.bin
/lib/firmware/brcm/brcmfmac43456-sdio.rockchip,rk3399-orangepi.bin
/lib/firmware/brcm/brcmfmac43456-sdio.radxa,rockpi4b.txt
/lib/firmware/brcm/brcmfmac43456-sdio.radxa,rockpi4c.txt
/lib/firmware/brcm/brcmfmac43456-sdio.radxa,zero.txt
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,pinebook-pro.txt
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,rockpro64-v2.1.txt
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,quartz64-a.txt
/lib/firmware/brcm/brcmfmac43456-sdio.pine64,quartz64-b.txt
/lib/firmware/brcm/brcmfmac43456-sdio.rockchip,rk3399-orangepi.txt
/lib/firmware/brcm/BCM4345C5.hcd

%changelog
* Tue Mar 15 2022 Dominik Mierzejewski <rpm@greysector.net> - 7.45.96.61-3
- add symlinks required by recent kernels

* Wed Nov 03 2021 Dominik Mierzejewski <rpm@greysector.net> - 7.45.96.61-2
- switch to manjaro wifi blobs, they were updated
- add support for more boards

* Tue May 11 2021 Dominik Mierzejewski <rpm@greysector.net> - 7.45.96.61-1
- update to 7.45.96.61 from armbian

* Mon May 10 2021 Dominik Mierzejewski <rpm@greysector.net> - 7.45.96.2-1
- initial build
